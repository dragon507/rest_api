from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://root:perroG0to@tallerapicrud.tk/flask_api'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False

db = SQLAlchemy(app)
ma = Marshmallow(app)

class task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(70), unique=True)
    description = db.Column(db.String(100))

    def __init__(self, title, description):
        self.title = title
        self.description = description

db.create_all()

class taskSchema(ma.Schema):
    class Meta:
        fields = ('id', 'title', 'description')

task_schema = taskSchema()
tasks_schema = taskSchema(many=True)

@app.route('/api/v1/Tasks', methods=['POST'])
def create_task():

    title = request.json['title']
    description = request.json['description']

    new_task = task(title, description)
    db.session.add(new_task)
    db.session.commit()

    return task_schema.jsonify(new_task)


@app.route('/api/v1/Tasks', methods=['GET'])
def get_tasks():
    all_tasks = task.query.all()
    result = tasks_schema.dump(all_tasks)
    return jsonify(result)

@app.route('/api/v1/Tasks/<id>', methods=['GET'])
def get_task(id):
    Task = task.query.get(id)
    return task_schema.jsonify(Task)

@app.route('/api/v1/Tasks/<id>', methods=['PUT'])
def update_task(id):
    task_u = task.query.get(id)

    title = request.json['title']
    description = request.json['description']

    task.title = title
    task.description =description

    db.session.commit()
    return task_schema.jsonify(task_u)

@app.route('/api/v1/Tasks/<id>', methods=['DELETE'])
def delete_task(id):
    task_d = task.query.get(id)
    db.session.delete(task_d)
    db.session.commit()

    return task_schema.jsonify(task_d)


@app.route('/', methods=['GET'])
def index():
    return jsonify({'message':'Welcomo to my Rest APi'})

if __name__ == '__main__':
    app.run(debug=True)
